# Scale9-4HTML

Combines testing of different options for using Scale9 grids for image scaling with work on custom JavaScript for working with Scale9 images.


# Prerequisites / Dependencies

This project is based on [Tal's Node Grunt Site Template](https://bitbucket.org/TheNonman/tals_nodegruntsitetemplate) and all of the same / usual requirements apply (for info on the features and support provided by the template, refer to its ReadMe):

1. [NodeJS](https://nodejs.org)

2. NodeJS NPM (installed with NodeJS)

4. [Ruby](https://www.ruby-lang.org)

5. The Ruby Compass Gem  (`gem install compass`)

6. The Grunt-CLI installed Globally  (`npm install -g grunt-cli`)

7.  *Optional*: Bower installed Globally  (`npm install -g bower`). The template itself makes no use of Bower, but is configured for it.

8.  *Optional*: The Karma-CLI installed Globally  (`npm install -g karma-cli`) The template itself currently makes no use of Karma, but is configured for it.

2. *Optional*: FontForge. Disabled by default Grunt-Webfont (AKA "SVG to webfont converter for Grunt") can run under either of two font rendering engines. Its default is to use a local install of FontForge (commandline). Its Node (only) engine option requires no additional system dependencies, but the author says it has problems with some SVG files and it has no support for ligatures. If you are going to want the better feature support via FontForge, you will need to install FontForge separetely. See "Installing FontForge and/or TTFAutoHint" below if needed.

2. *Optional*: ttfautohint. For proper hinting by Grunt-Webfont. See "Installing FontForge and/or TTFAutoHint" below if needed.

2. *Optional*: OpenFL. Disabled by default, the template's gruntfile has support for including an OpenFL compiling step in your build process. To enable this, see the "Enabling OpenFL" section below.

# Getting Started

Assuming that you have the prerequisites installed on your environment...

1. Sync this Git repo

2. Open a Command Line / Terminal targeting the local repo directory.

    1. Execute `npm install`
    4. Execute: `grunt Work`

Grunt will build a Staging site from the content in AppSource, launch that Staging site via Express, bind it up with BrowserSync and open the site in Firefox, Chrome and Safari (as available) with dev watches set for changes. Note: on Windows, you should get a dialog at least about a failure to run Safari (naturally), but you'll likely see an issue with Chrome as well... in the gruntfile.js find the BrowserSync task config (remove Safari) and change "Google Chrome" to just "Chrome."

When ready, use Cntrl-C to shut down the server and watches.


# Build and Test

As noted above, the Work grunt task (executed with `grunt Work`) will build a fully navigatable and testable Staging environment using a complete "source to temp to staging" build flow. It will then spin up a NodeJS Express web server bound to the produced staging environment ready to be accessed.

The Express web server is set by default to using port 3000. This can be changed by simply editing the Express server configuration in 'ServerConfigs/server_app.js'.

Of course, if you change the main server port, you will want to also change the server URL that BrowserSync is set to proxy appropriately. This can be changed in the gruntfile. Just change in in the 'options' object of the 'browserSync' task definition.


# Grunt Task Help

Execute `grunt Help` or just `grunt` to get a list of available tasks, with those tasks intended to be called directly by users specifically called out.


# Additional Documentation

Additional info and documentation can be found in the repo's 'Documentation' directory.


# Markdown and ReadMe References

If you want to learn more about editing this ReadMe and creating good markdown-based readme files, refer the following sources:

* [Adam Pritchard's Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

* [Markdown-Guide](http://markdown-guide.readthedocs.io/en/latest/basics.html)

* [Visual Studio ReadMe Guidelines](https://www.visualstudio.com/en-us/docs/git/create-a-readme)


