package;

import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.display.Tilemap;
import openfl.display.Tileset;
import openfl.events.Event;
import openfl.geom.Rectangle;
import openfl.Assets;

import com.kircode.debug.FPS_Mem;


class Main extends Sprite {

	var tileset:Tileset;
	var tilemap:Tilemap;
	var screen:Sprite;
	var particles:Array<Particle>;
	var pID:Int;

	public function new () {

		super ();

		var bitmapInst1 = new Bitmap (Assets.getBitmapData ("assets/openfl.png"));

		addChild (bitmapInst1);

		bitmapInst1.x = (stage.stageWidth - bitmapInst1.width) / 2;
		bitmapInst1.y = (stage.stageHeight - bitmapInst1.height) / 2;

		init();
	}

	function init (  )
	{
		//screen = new Sprite();
		//addChild(screen);

		particles = new Array<Particle>();

		var particle:BitmapData = new BitmapData(4, 4, false, 0x0000ff);
		tileset = new Tileset(particle);
		pID = tileset.addRect(new Rectangle(0, 0, 4, 4));
		tilemap = new Tilemap(stage.stageWidth, stage.stageHeight, tileset);

		addChild(tilemap);

		var fpsDisplay:Sprite = new Sprite();
		var fps_mem:FPS_Mem = new FPS_Mem(10, 10, 0xffffff);
		var background:Sprite = new Sprite();
		background.graphics.beginFill(0x000000, 0.7);
		background.graphics.drawRect(0, 0, fps_mem.width, fps_mem.height);
		background.graphics.endFill();
		fpsDisplay.addChild(background);
		fpsDisplay.addChild(fps_mem);

		addChild(fpsDisplay);
		fpsDisplay.x = stage.stageWidth - fpsDisplay.width;

		this.addEventListener(Event.ENTER_FRAME, update);
	}


	public function update ( e:Event )
	{
		for (i in 0...10) {
			var newParticle:Particle = new Particle();
			newParticle.x = 0;
			newParticle.y = 0;
			newParticle.acc_x = 0;
			newParticle.acc_y = 0.5;
			newParticle.vel_x = Math.random() * 7 + 3;
			newParticle.vel_y = Math.random() * 5;

			particles.push(newParticle);
			tilemap.addTile(newParticle);
		}
		// add particle

		// render
		//screen.graphics.clear();
		var tileData:Array<Float> = new Array<Float>();
		var particle:Particle;
		for (particle in particles) {
			if (particle.x > stage.stageWidth) {

				tilemap.removeTile(particle);
				particles.remove(particle);
			} else {
				tileData = tileData.concat([particle.x, particle.y, 0]);

				particle.x += particle.vel_x;
				particle.y += particle.vel_y;
				particle.vel_x += particle.acc_x;
				particle.vel_y += particle.acc_y;
				if (particle.y > stage.stageHeight) {
					particle.y = stage.stageHeight;
					particle.vel_y *= -Math.random()*0.7;
				}
			}
		}

	}


}
