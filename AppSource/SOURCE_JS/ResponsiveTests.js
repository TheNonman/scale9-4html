'use strict';

(function () {
	console.log("[SimpleTests]  Loaded and Starting...");

	console.log("[SimpleTests]  Window: " + window);
	console.log("[SimpleTests]  Window InnerWidth: " + window.innerWidth);

	var theDivList = document.getElementsByClassName('test_div');
	console.log("[SimpleTests]  Test Div: " + theDivList[0]);
	console.log("[SimpleTests]  Test Div Width: " + theDivList[0].clientWidth);

	window.setInterval(function(){
		if (window.innerWidth < 1000) {
			console.log("window small");
		}
		else {
			console.log("window large");
		}
	}
	,500
	);
}
)();
