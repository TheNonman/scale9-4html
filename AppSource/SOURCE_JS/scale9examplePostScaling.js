/* globals Texture_Scale9 */
'use strict';

(function () {

	var Example = class Example {
		constructor (  )
		{
			// event handlers
			this.assetsLoadedHandler = this.assetsLoadedHandler.bind(this);
			this.resizeImageHandler = this.resizeImageHandler.bind(this);
			this.enableResizeImageHandler = this.enableResizeImageHandler.bind(this);
			this.disableResizeImageHandler = this.disableResizeImageHandler.bind(this);
			this.img = new Image();
			this.img.onload = this.assetsLoadedHandler;
			this.context = (document.getElementById("canvas2")).getContext("2d");

			this.AllowSmoothing = true;
			this.context.mozImageSmoothingEnabled = this.AllowSmoothing;
			this.context.webkitImageSmoothingEnabled = this.AllowSmoothing;
			this.context.msImageSmoothingEnabled = this.AllowSmoothing;
			this.context.imageSmoothingEnabled = this.AllowSmoothing;
			this.context.imageSmoothingQuality = "high";
		}


		init ( src )
		{
			this.img.src = "./img/achievements_dialog.png"; // calls setup when loaded

			return this;
		}


		setup ( )
		{
			this.scaler = new Texture_Scale9(this.context, this.img, 6, 20, 2, 2, 3);
			this.scaler.Mode = this.scaler.Mode_Pixel;
			this.scaler.SmoothingQuality = this.scaler.Smoothing_Low;

			this.scaler2 = new Texture_Scale9(this.context, this.img, 6, 20, 2, 2, 3);
			this.scaler2.Mode = this.scaler2.Mode_Pixel;
			this.scaler2.SmoothingQuality = this.scaler2.Smoothing_High;

			this.scaler3 = new Texture_Scale9(this.context, this.img, 6, 20, 2, 2, 3);
			this.scaler3.Mode = this.scaler3.Mode_Smooth;
			this.scaler3.SmoothingQuality = this.scaler3.Smoothing_High;

			this.draw(this.img.width, this.img.height);
			this.context.canvas.addEventListener("mousedown", this.enableResizeImageHandler);

			return document.addEventListener("mouseup", this.disableResizeImageHandler);
		}


		draw ( w, h )
		{
			// Full Clear of the Canvas
			this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);

			// Render the unscaled image for comp and testing
			//this.context.globalAlpha = 0.25;
			//this.context.drawImage(this.img, 0, 0);

			this.context.globalAlpha = 1.0;
			this.scaler.resize(Math.ceil(w), Math.ceil(h));
			this.scaler2.resize(65, 100);
			this.scaler3.resize(65, 100);

			this.scaler.drawImageTo(this.context, 0, 0);
			this.scaler2.drawImageTo(this.context, 50, 0);
			this.scaler3.drawImageTo(this.context, 100, 50);
		}


		assetsLoadedHandler ( event )
		{
			return this.setup();
		}


		resizeImageHandler ( event )
		{
			var elem, mouseX, mouseY, rect;
			// resize based on mouse position (position is size)
			elem = this.context.canvas;
			rect = elem.getBoundingClientRect();
			mouseX = event.clientX + elem.scrollLeft - elem.clientLeft - rect.left;
			mouseY = event.clientY + elem.scrollTop - elem.clientTop - rect.top;

			return this.draw(mouseX/3, mouseY/3);
		}


		enableResizeImageHandler ( event )
		{
			document.addEventListener("mousemove", this.resizeImageHandler);

			return event.preventDefault();
		}


		disableResizeImageHandler ( event )
		{
			return document.removeEventListener("mousemove", this.resizeImageHandler);
		}
	};// END  CLASS

	new Example().init();

})();
