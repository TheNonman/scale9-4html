
'use strict';

(function () {

	var CanvasExample = class Example {
		constructor (  )
		{
			// event handlers
			this.assetsLoadedHandler = this.assetsLoadedHandler.bind(this);

			this.img = new Image();
			this.img.onload = this.assetsLoadedHandler;

			this.context = (document.getElementById("canvas2")).getContext("2d");
		}


		init ( src )
		{
			console.log("CanvasExample :: Init()");
			console.log(" - img: " + this.img);
			console.log(" - context: " + this.context);

			this.img.src = "./img/achievements_dialog.png"; // calls setup when loaded

		}


		setup ( )
		{
			console.log("CanvasExample :: setup()");
			console.log(" - img width: " + this.img.width);
			console.log(" - img height: " + this.img.height);

			// Use Nearest-Neighbor Interpolation
			this.AllowSmoothing = false;
			this.context.mozImageSmoothingEnabled = this.AllowSmoothing;
			this.context.webkitImageSmoothingEnabled = this.AllowSmoothing;
			this.context.msImageSmoothingEnabled = this.AllowSmoothing;
			this.context.imageSmoothingEnabled = this.AllowSmoothing;
			this.context.imageSmoothingQuality = "high";

			this.context.drawImage(this.img, 0, 0, this.img.width, this.img.height,
									0, 0, this.img.width * 3, this.img.height * 3);


			// Use Nearest-Neighbor Interpolation
			this.AllowSmoothing = true;
			this.context.mozImageSmoothingEnabled = this.AllowSmoothing;
			this.context.webkitImageSmoothingEnabled = this.AllowSmoothing;
			this.context.msImageSmoothingEnabled = this.AllowSmoothing;
			this.context.imageSmoothingEnabled = this.AllowSmoothing;
			this.context.imageSmoothingQuality = "high";

			this.context.drawImage(this.img, 0, 0, this.img.width, this.img.height,
									100, 100, this.img.width * 3, this.img.height * 3);
			/*
			this.draw(this.img.width, this.img.height);
			this.context.canvas.addEventListener("mousedown", this.enableResizeImageHandler);

			return document.addEventListener("mouseup", this.disableResizeImageHandler);
			*/
		}


		draw ( w, h )
		{
			// Full Clear of the Canvas
			this.context.clearRect(0, 0, this.context.canvas.width, this.context.canvas.height);

			// Render the unscaled image for comp and testing
			//this.context.globalAlpha = 0.25;
			//this.context.drawImage(this.img, 0, 0);

			this.context.globalAlpha = 1.0;
			this.scaler.resize(Math.ceil(w), Math.ceil(h));

			this.AllowSmoothing = true;
			this.context.mozImageSmoothingEnabled = this.AllowSmoothing;
			this.context.webkitImageSmoothingEnabled = this.AllowSmoothing;
			this.context.msImageSmoothingEnabled = this.AllowSmoothing;
			this.context.imageSmoothingEnabled = this.AllowSmoothing;
			this.context.imageSmoothingQuality = "high";

			return this.scaler.drawImageTo(this.context, 0, 0);
		}


		assetsLoadedHandler ( event )
		{
			this.setup();
		}


		resizeImageHandler ( event )
		{
			var elem, mouseX, mouseY, rect;
			// resize based on mouse position (position is size)
			elem = this.context.canvas;
			rect = elem.getBoundingClientRect();
			mouseX = event.clientX + elem.scrollLeft - elem.clientLeft - rect.left;
			mouseY = event.clientY + elem.scrollTop - elem.clientTop - rect.top;

			return this.draw(mouseX, mouseY);
		}


		enableResizeImageHandler ( event )
		{
			document.addEventListener("mousemove", this.resizeImageHandler);

			return event.preventDefault();
		}


		disableResizeImageHandler ( event )
		{
			return document.removeEventListener("mousemove", this.resizeImageHandler);
		}
	};// END  CLASS

	new CanvasExample().init();

})();
