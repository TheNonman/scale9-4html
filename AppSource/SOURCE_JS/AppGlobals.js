/* exports AppGlobals */
'use strict';

console.log("AppGlobals Init()");
var AppGlobals = AppGlobals || {};

// Useful RegEx Library
AppGlobals.RegExLib = {};

// URL Validation

// Protocol Optional (but only http and https allowed)
// NO Query Allowed
// Fragment Allowed (i.e. '#SomeFragment')
// Case Insensative
AppGlobals.RegExLib.URL_OptionalProtocol_NoQuery = /^(https?:\/\/)?((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|((\d{1,3}\.){3}\d{1,3}))(\:\d+)?(\/[-a-z\d%_.~+]*)*(\#[-a-z\d_]*)?$/i;


// Protocol Optional (but only http and https allowed)
// Query Allowed
// Fragment Allowed (i.e. '#SomeFragment')
// Case Insensative
AppGlobals.RegExLib.URL_OptionalProtocol_AllowQuery = /^(https?:\/\/)?((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|((\d{1,3}\.){3}\d{1,3}))(\:\d+)?(\/[-a-z\d%_.~+]*)*(\?[;&a-z\d%_.~+=-]*)?(\#[-a-z\d_]*)?$/i;



// Protocol Required (must be either http or https)
// NO Query Allowed
// Fragment Allowed (i.e. '#SomeFragment')
// Case Insensative
AppGlobals.RegExLib.URL_NoQuery = /^(https?:\/\/)((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|((\d{1,3}\.){3}\d{1,3}))(\:\d+)?(\/[-a-z\d%_.~+]*)*(\#[-a-z\d_]*)?$/i;


// Protocol Required (must be either http or https)
// Query Allowed
// Fragment Allowed (i.e. '#SomeFragment')
// Case Insensative
AppGlobals.RegExLib.URL_AllowQuery = /^(https?:\/\/)((([a-z\d]([a-z\d-]*[a-z\d])*)\.)+[a-z]{2,}|((\d{1,3}\.){3}\d{1,3}))(\:\d+)?(\/[-a-z\d%_.~+]*)*(\?[;&a-z\d%_.~+=-]*)?(\#[-a-z\d_]*)?$/i;


//
//
// Collection of Utility Methods
// =============================================================
// =============================================================

AppGlobals.Utils = AppGlobals.Utils || {};

AppGlobals.Utils.HasStringContent = function ( varToCheck )
{
	if (typeof varToCheck === 'string') {
		var trimmedVar = varToCheck.trim();
		return (trimmedVar.length > 0);
	}
	return false;
};


AppGlobals.Utils.SortJSON_ArrayAlphabetical_ByKey = function ( theArray, theKey )
{
	if (!Array.isArray(theArray) || !AppGlobals.Utils.HasStringContent(theKey)) {
		console.log("AppGlobals :: ERROR @ Utils.SortJSON_ArrayAlphabetical_ByKey()");
		console.log("--> INVALID Arguments Received! Request TERMINATED - Returning NULL !!!");
		return null;
	}
	if (theArray.length <=1)	return theArray;
	if (!theArray[0].hasOwnProperty(theKey)) {
		console.log("AppGlobals :: ERROR @ Utils.SortJSON_ArrayAlphabetical_ByKey()");
		console.log("--> Array Does Not Have the Provided Key: " + theKey);
		console.log("--> Returning Unmodified Array !");
		return theArray;
	}

	return theArray.sort(function (a, b) {
		var nameA = a[theKey].toLowerCase();
		var nameB = b[theKey].toLowerCase();
		if (nameA < nameB)		return -1;
		if (nameA > nameB)		return 1;
		return 0;
	});
};


AppGlobals.Utils.NewJSON_Array_ByKey = function ( theArray, theKey, theKeyValue )
{
	if (!Array.isArray(theArray) || !AppGlobals.Utils.HasStringContent(theKey)) {
		console.log("AppGlobals :: ERROR @ Utils.NewJSON_Array_ByKey()");
		console.log("--> INVALID Arguments Received! Request TERMINATED - Returning NULL !!!");
		return null;
	}
	if (theArray.length <=1)	return theArray;
	if (!theArray[0].hasOwnProperty(theKey)) {
		console.log("AppGlobals :: ERROR @ Utils.NewJSON_Array_ByKey()");
		console.log("--> Array Does Not Have the Provided Key: " + theKey);
		console.log("--> Returning Unmodified Array !");
		return theArray;
	}

	var newArray = theArray.filter(function(aryItem){
		return (aryItem[theKey] === theKeyValue);
	});

	return newArray;
};


AppGlobals.Utils.DateFromISOTimestamp = function ( aTimestamp )
{
	if (!AppGlobals.Utils.HasStringContent(aTimestamp)) {
		console.log("AppGlobals :: ERROR @ Utils.DateFromTimestamp() : INVALID Argument Received! Request TERMINATED - Returning NULL !!!");
		return null;
	}
	if (aTimestamp.lastIndexOf("-") < 10 && aTimestamp.indexOf("+") === -1) {
		aTimestamp += "-08:00";
		console.log("No + or - found in ISO Timestamp... Appending US Pacific TimeZone Adjustment (-08:00)... Timestamp: " + aTimestamp);
	}

	var jsDate = new Date(Date.parse(aTimestamp));

	return jsDate;
};
