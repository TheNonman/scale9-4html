/*
//
// Copyright (c) 2018 Tom Keen
// MIT License (MIT)
//
// Based on canvas-Scale9 by Trevor McCauley (senocular) (https://github.com/senocular/canvas-Scale9)
// MIT License (MIT)
// 
// Copyright (c) 2013 Trevor McCauley
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//

Description
///////////////////////////////////////////////////////////////

A Vanilla Javascript class for instantiating and working with images in Scale 9
(AKA 9 sliced) on the HTML5 Canvas.

Includes support for post-processing a targeted per pixel scale increase ('texels') 
and changing the canvas rendering mode (i.e. smooth vs nearest neighbor scaling) as
needed.

*/

'use strict';

(function () {

	var Texture_Scale9 = class Texture_Scale9
	{
		constructor ( renderContext, image, x, y, width, height, preScale )
		{
			this.renderContext = renderContext;
			this.mode = this.Mode_Smooth;
			this.smoothingQuality = this.Smoothing_High;
			this.savedRendererSettings = {};

			this.image = image;
			this.preScale = (preScale != undefined ? preScale : 1);
			console.log("Texture_Scale9 PixelScale: " + this.preScale);

			this.setCenterRect(x, y, width, height);
			this.resize(this.image.width, this.image.height);
		}

		get Mode (  ) {
			return this.mode;
		}

		set Mode ( renderMode ) {
			this.mode = renderMode;
		}

		get Mode_Smooth (  ) {
			return 0;
		}

		get Mode_Pixel (  ) {
			return 1;
		}

		get SmoothingQuality (  ) {
			return this.smoothingQuality;
		}

		set SmoothingQuality ( renderQuality ) {
			this.smoothingQuality = renderQuality;

		}

		get Smoothing_Low (  ) {
			return "low";
		}

		get Smoothing_Med (  ) {
			return "medium";
		}

		get Smoothing_High (  ) {
			return "high";
		}


		setCenterRect ( centerRectX, centerRectY, centerRectW, centerRectH )
		{
			this.centerRectX = centerRectX;
			this.centerRectY = centerRectY;
			this.centerRectW = centerRectW;
			this.centerRectH = centerRectH;
		}


		getCenterRect (  )
		{
			return {
				x: this.centerRectX,
				y: this.centerRectY,
				width: this.centerRectW,
				height: this.centerRectH
			};
		}


		resize ( width1, height1 )
		{
			this.width = width1;
			this.height = height1;
		}


		scale ( scaleX, scaleY = scaleX )
		{
			return this.resize(Math.ceil(scaleX * this.image.width), Math.ceil(scaleY * this.image.height));
		}


		drawImageTo ( context, x, y )
		{
			var bottomH, bottomY, centerH, centerW, centerX, centerY,
				clipBottomH, clipBottomY, clipRightW, clipRightX,
				leftW, ratio, rightW, rightX, topH;

			// make sure the rect values are
			// within the bounds of the image
			// this is done here since the state of
			// the image could change at any time
			this._validateRect();

			// slice dimensions
			// note some specific dimensions (such as
			// clipCenterX) are not defined since they
			// will always match a previously defined
			// member (such as @centerRectX)
			clipRightX = this.centerRectX + this.centerRectW;
			leftW = this.centerRectX;
			rightW = this.image.width - clipRightX;

			centerW = this.width - (leftW + rightW);
			clipRightW = rightW;
			clipBottomY = this.centerRectY + this.centerRectH;
			topH = this.centerRectY;
			bottomH = this.image.height - clipBottomY;
			centerH = this.height - (topH + bottomH);
			clipBottomH = bottomH;
			// make adjustments if resized below the available
			// dimensions of the resizable (center) areas
			if (centerW < 0) {
				centerW = 0;
				if (leftW === 0) {
					rightW = this.width;
				}
				else if (rightW === 0) {
					leftW = this.width;
				}
				else {
					// scale proportionally
					ratio = leftW / rightW;
					rightW = Math.ceil(this.width / (ratio + 1));
					leftW = this.width - rightW;
				}
			}
			if (centerH < 0) {
				centerH = 0;
				if (topH === 0) {
					bottomH = this.height;
				}
				else if (bottomH === 0) {
					topH = this.height;
				}
				else {
					// scale proportionally
					ratio = topH / bottomH;
					bottomH = Math.ceil(this.height / (ratio + 1));
					topH = this.height - bottomH;
				}
			}
			// additional dimensions after adjustments
			centerX = x + leftW;
			centerY = y + topH;
			rightX = x + leftW + centerW;
			bottomY = y + topH + centerH;

			// Store the Active Render Context Settings and set the Configure Values for Rendering this
			this.saveCurrentRenderSettings();
			this.setRenderSettings();

			// perform the various drawImage calls to
			// draw each of the 9 slices (where visible)
			if (leftW > 0) {
				if (topH > 0) {
					context.drawImage(	this.image, 0, 0, this.centerRectX, this.centerRectY,
										x * this.preScale, y * this.preScale, leftW * this.preScale, topH * this.preScale);
				}
				if (centerH > 0) {
					context.drawImage(	this.image, 0, this.centerRectY, this.centerRectX, this.centerRectH,
										x * this.preScale, centerY * this.preScale, leftW * this.preScale, centerH * this.preScale);
				}
				if (bottomH > 0) {
					context.drawImage(	this.image, 0, clipBottomY, this.centerRectX, clipBottomH,
										x * this.preScale, bottomY * this.preScale, leftW * this.preScale, bottomH * this.preScale);
				}
			}
			if (centerW > 0) {
				if (topH > 0) {
					context.drawImage(	this.image, this.centerRectX, 0, this.centerRectW, this.centerRectY,
										centerX * this.preScale, y * this.preScale, centerW * this.preScale, topH * this.preScale);
				}
				if (centerH > 0) {
					context.drawImage(	this.image, this.centerRectX, this.centerRectY, this.centerRectW, this.centerRectH,
										centerX * this.preScale, centerY * this.preScale, centerW * this.preScale, centerH * this.preScale);
				}
				if (bottomH > 0) {
					context.drawImage(	this.image, this.centerRectX, clipBottomY, this.centerRectW, clipBottomH,
										centerX * this.preScale, bottomY * this.preScale, centerW * this.preScale, bottomH * this.preScale);
				}
			}
			if (rightW > 0) {
				if (topH > 0) {
					context.drawImage(	this.image, clipRightX, 0, clipRightW, this.centerRectY,
										rightX * this.preScale, y * this.preScale, rightW * this.preScale, topH * this.preScale);
				}
				if (centerH > 0) {
					context.drawImage(	this.image, clipRightX, this.centerRectY, clipRightW, this.centerRectH,
										rightX * this.preScale, centerY * this.preScale, rightW * this.preScale, centerH * this.preScale);
				}
				if (bottomH > 0) {
					return context.drawImage(	this.image, clipRightX, clipBottomY, clipRightW, clipBottomH,
												rightX * this.preScale, bottomY * this.preScale, rightW * this.preScale, bottomH * this.preScale);
				}
			}

			// After Rendering/Drawing This, Restore the Previous Render Settings
			this.restoreRenderSettings();
		}


		restoreRenderSettings (  )
		{
			this.renderContext.mozImageSmoothingEnabled = this.savedRendererSettings.mozImageSmoothingEnabled;
			this.renderContext.webkitImageSmoothingEnabled = this.savedRendererSettings.webkitImageSmoothingEnabled;
			this.renderContext.msImageSmoothingEnabled = this.savedRendererSettings.msImageSmoothingEnabled;
			this.renderContext.imageSmoothingEnabled = this.savedRendererSettings.imageSmoothingEnabled;
			this.renderContext.imageSmoothingQuality = this.savedRendererSettings.imageSmoothingQuality;
		}


		saveCurrentRenderSettings (  )
		{
			this.savedRendererSettings.mozImageSmoothingEnabled = this.renderContext.mozImageSmoothingEnabled;
			this.savedRendererSettings.webkitImageSmoothingEnabled = this.renderContext.webkitImageSmoothingEnabled;
			this.savedRendererSettings.msImageSmoothingEnabled = this.renderContext.msImageSmoothingEnabled;
			this.savedRendererSettings.imageSmoothingEnabled = this.renderContext.imageSmoothingEnabled;
			this.savedRendererSettings.imageSmoothingQuality = this.renderContext.imageSmoothingQuality;
		}

		setRenderSettings (  )
		{
			if (this.mode === this.Mode_Smooth) {
				this.renderContext.mozImageSmoothingEnabled =
				this.renderContext.webkitImageSmoothingEnabled =
				this.renderContext.msImageSmoothingEnabled =
				this.renderContext.imageSmoothingEnabled = true;
			}
			else if (this.mode === this.Mode_Pixel) {
				this.renderContext.mozImageSmoothingEnabled =
				this.renderContext.webkitImageSmoothingEnabled =
				this.renderContext.msImageSmoothingEnabled =
				this.renderContext.imageSmoothingEnabled = false;
			}
			else {
				console.log("[Texture_Scale9] :: ERROR @ setRenderSettings(); Unsupported Mode ("+ this.mode +") Render Settings Unchanged !");
			}

			this.savedRendererSettings.imageSmoothingQuality = this.smoothingQuality;
		}


		_validateRect (  )
		{
			var maxH, maxW;
			if (this.centerRectX < 0) {
				this.centerRectX = 0;
			}
			if (this.centerRectX > this.image.width) {
				this.centerRectX = this.image.width;
			}
			if (this.centerRectY < 0) {
				this.centerRectY = 0;
			}
			if (this.centerRectY > this.image.height) {
				this.centerRectY = this.image.height;
			}
			if (this.centerRectW < 0) {
				this.centerRectW = 0;
			}

			maxW = this.image.width - this.centerRectX;
			if (this.centerRectW > maxW) {
				this.centerRectW = maxW;
			}
			if (this.centerRectH < 0) {
				this.centerRectH = 0;
			}

			maxH = this.image.height - this.centerRectY;
			if (this.centerRectH > maxH) {
				this.centerRectH = maxH;
			}
		}
	};// End  Class  Def

	// making the definition globally available
	window.Texture_Scale9 = Texture_Scale9;

})();
